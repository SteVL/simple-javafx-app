package com.stevl.simplejavafxapp;

import java.io.*;

/**
 * Работает с файлами
 */
public class FileWorker {

    private final File file;

    public FileWorker(File file) {
        this.file = file;
    }

    /**
     * Записывает тип задачи и входные данные в текстовый файл
     *
     * @param taskType тип задачи
     * @param data     входные данные
     * @throws IOException
     */
    public void writeToTxt(final String taskType, final String data) throws IOException {
        FileWriter fileWriter;
        BufferedWriter bufWriter;
        //Создаём буферизированный поток на запись
        fileWriter = new FileWriter(file);
        bufWriter = new BufferedWriter(fileWriter);
        //запись типа задачи и переход на новую строку
        bufWriter.write(taskType + "\n");
        //запись входных данных как есть.
        bufWriter.write(data);
        bufWriter.close(); // закрываем поток
    }

    /**
     * Читает текстовый файл
     *
     * @return Строковый массив, где 1-ый элемент - тип задачи, 2 - входные данные
     */
    public String[] loadFromTxt() throws IOException {
        //Создаём буферизированный поток на чтение
        FileReader fileReader = new FileReader(file);
        BufferedReader bufReader = new BufferedReader(fileReader);

        String line;
        StringBuilder sbl = new StringBuilder();
        while ((line = bufReader.readLine()) != null) {
            sbl.append(line).append("\n");
        }
        //По протоколу, может быть массив из 3 строк или из 2-х, где 3 - это 1 задача, 2 - это 2 задача
        String[] resArrStr = sbl.toString().split("\n");
        //проверим на 3 строки. Есть да,то видоизменим данные в удобный формат.
        resArrStr = defineData(resArrStr);
//        DataConverter.printArrStr(resArrStr);
        //закрытие потока на чтение
        bufReader.close();
        return resArrStr;
    }

    /**
     * Служебный метод для подготовки данных
     * @param arrStr
     * @return
     */
    private String[] defineData(String[] arrStr) {
        String[] resArr = new String[2];
        if (arrStr.length == 3) {
            //если 3 строки в массиве, то данные из файла задачи 1.
            //берём тип задачи
            resArr[0] = arrStr[0];
            //вставляем разделитель между первой и второй строкой входных данных
            resArr[1] = arrStr[1] + "\n" + arrStr[2];
        } else {
            //иначе, оставляем как есть
            resArr = arrStr;
        }
        return resArr;
    }
}


