package com.stevl.simplejavafxapp;

import com.stevl.simplejavafxapp.exceptions.MoreOneSpaceException;

/**
 * Конвертирует данные из формы в данные для решения задачи. Что-то вроде контроллера.
 */
public class DataConverter {

    /**
     * Конвертирует данные из формы в данные для решения задачи 1.
     * @param dataFromTextArea
     * @return
     * @throws MoreOneSpaceException больше одного пробела в строке между словами.
     */
    public String[][] prepareDataForTask1(String dataFromTextArea) throws MoreOneSpaceException {
        String originalStr = dataFromTextArea;
        String[][] resArrStr = new String[2][];
        try {
            //данные ожидаются в виде двух строк, где слова разделены пробелом
            //первая строка - строка-шаблон
            //вторая строка - рабочая
            //убираем боковые пробелы (если есть) и делим строку на две строки по символу переноса.
            String[] twoStrings = dataFromTextArea.trim().split("\n");
//            printArrStr(twoStrings);
            //нужна проверка на случай, если внутри строки больше одного пробела (позже)
            for (int i = 1; i < dataFromTextArea.length() - 1; i++) {
                if (dataFromTextArea.charAt(i + 1) == ' '
                        && dataFromTextArea.charAt(i) == ' ') {
                    System.out.println("Пробелы:" + i + ", " + (i + 1));
                    throw new MoreOneSpaceException("The string contains more than one space in a row.");
                }
            }

            for (int i = 0; i < resArrStr.length; i++) {
                //разбиваем строку на массив строк по пробелу
                String[] arrStr = twoStrings[i].split(" ");
                //"заполняем" массив результирующего [][] массива полученным массивом
                resArrStr[i] = arrStr;
//                printArrStr(resArrStr[i]);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return resArrStr;
    }

    /**
     * Конвертирует данные из формы в данные для решения задачи. Что-то вроде контроллера.
     * @param dataFromTextArea
     * @return
     */
    public String prepareDataForTask2(String dataFromTextArea){
            //минимум проверки.
            //пока что обрубим боковые пробелы (если есть)
            //остальное - если время будет.
            return dataFromTextArea.trim();
    }


    //Util methods

    /**
     * Служебный. Выводит на экран строки из массива
     * @param arrStr
     */
    public static void printArrStr(String[] arrStr) {
        System.out.println("arrStrLength:" + arrStr.length);
        for (int i = 0; i < arrStr.length; i++) {
            System.out.print(arrStr[i] + "\n");
        }
        lengthEachStr(arrStr);
    }

    //вывести длины каждой строки массива

    /**
     * Служебный. Выводит на экран длины строк массива
     * @param arrStr
     */
    public static void lengthEachStr(String[] arrStr) {
        System.out.println("==== lengths each str ====");
        for (int i = 0; i < arrStr.length; i++) {
            System.out.println(arrStr[i] + "=" + arrStr[i].length());
        }

    }
    //End Util methods
}
