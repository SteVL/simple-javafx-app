package com.stevl.simplejavafxapp.tasks;

import com.stevl.simplejavafxapp.DataConverter;

import javax.xml.crypto.Data;

/**
 * Описывает решение задачи из файла 2.txt
 * Write Number in Expanded Form
 * <p>
 * You will be given a number and you will need to return it as a string in Expanded Form. For example:
 * <p>
 * expanded(12); # Should return "10 + 2"
 * expanded(42); # Should return "40 + 2"
 * expandedm(70304); # Should return "70000 + 300 + 4"
 * NOTE: All numbers will be whole numbers greater than 0.
 */
public class Task2 {
    /**
     * Входное целое число
     */
    private final String inputedWholeNumber;

    public Task2(String inputedWholeNumber) {
        this.inputedWholeNumber = inputedWholeNumber;
    }

    /**
     * Раскладывает целое число, на составляющие разряды.
     *
     * @return строка вида "7000+300+40+1"
     */
    public String expandWholeNumber() {
        //массив куда будут записываться разряды
        String[] tempArrStr = new String[inputedWholeNumber.length()];
        // число нулей
        int numZero = 0;

        for (int cnt = inputedWholeNumber.length() - 1; cnt >= 0; cnt--) {
            //c конца к началу берём символ, записываем его в массив и прибавляем нужно число нулей.
            tempArrStr[cnt] = inputedWholeNumber.substring(cnt, cnt + 1) + getZero(numZero);
            System.out.println(tempArrStr[cnt]);
            numZero++;
        }
//        DataConverter.printArrStr(tempArrStr);
        //добавляем плюсики между разрядами
        StringBuilder sbl=new StringBuilder();
        for (int i = 0; i <tempArrStr.length ; i++) {
            sbl.append(tempArrStr[i]).append("+");
        }
        //удаляем ненужный плюс в конце
        sbl.deleteCharAt(sbl.length()-1);

        String expandedNumber = sbl.toString();
//        System.out.println(expandedNumber);
        return expandedNumber;
    }

    /**
     * Строит строку из нулей исходя из заданного количества
     *
     * @return
     */
    private String getZero(int numZero) {
        if (numZero == 0) return "";
        else {
            StringBuilder sbl = new StringBuilder();
            for (int i = 0; i < numZero; i++) {
                sbl.append("0");
            }
            return sbl.toString();
        }
    }
}
