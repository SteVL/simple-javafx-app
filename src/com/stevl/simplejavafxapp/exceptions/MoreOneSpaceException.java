package com.stevl.simplejavafxapp.exceptions;

/**
 * Исключение: когда строка содержит более одного пробела подряд между словами.
 * Боковые пробелы не влияют.
 */
public class MoreOneSpaceException extends Exception {

    public MoreOneSpaceException(String message) {
        super(message);
    }
}
