package com.stevl.simplejavafxapp;

import com.stevl.simplejavafxapp.exceptions.MoreOneSpaceException;
import com.stevl.simplejavafxapp.tasks.Task1;
import com.stevl.simplejavafxapp.tasks.Task2;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class ViewController implements Initializable {

    public final static String APP_TITLE = "Simple JavaFX Application";

    @FXML
    private MenuBar menuBar;
    @FXML
    private Button saveButton;
    @FXML
    private Button loadButton;
    @FXML
    private ComboBox taskComboBox;
    @FXML
    private Button calculateButton;

    @FXML
    private TextArea inputDataTextArea, resultDataTextArea;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        taskComboBox.setPromptText("Задача...");
        taskComboBox.getItems().addAll("Задача 1", "Задача 2");
        inputDataTextArea.setText("");
        resultDataTextArea.setText("");
    }

    @FXML
    /**
     * Корректный выход при нажатии на Файл -> Выход
     */
    private void exit() {
        System.out.println("exit from javafx app");
        Stage stage = (Stage) menuBar.getScene().getWindow();
        stage.close();
    }

    @FXML
    /**
     * Обработчик нажатия кнопки сохранения
     */
    private void saveButtonPressed() {
        //Подумать, можно ли это вынести в отдельную часть, чтобы не было громоздкого дублирования.
        //проверка выбора типа задачи
        int selectedTaskIndex = taskComboBox.getSelectionModel().getSelectedIndex();
        if (selectedTaskIndex == -1) {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle(APP_TITLE);
            alert.setHeaderText("Не выбрана задача");
            alert.setContentText("Выберите задачу из выпадающего списка");
            alert.showAndWait();
            return;
        }
        //проверка заполненности текстовой области с входными данными
        if (inputDataTextArea.getText().isEmpty()) {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle(APP_TITLE);
            alert.setHeaderText("Не заполнено поле входных данных");
            alert.setContentText("Заполните поле или выполните загрузку данных");
            alert.showAndWait();
            return;
        }
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Сохранить как");
        //установка фильтра только на текстовые файлы
        FileChooser.ExtensionFilter extFilter =
                new FileChooser.ExtensionFilter("Текстовые файлы (*.txt)", "*.txt");
        fileChooser.getExtensionFilters().add(extFilter);
        Stage window = (Stage) saveButton.getScene().getWindow();
        //получение файла (пути) из диалога
        File file = fileChooser.showSaveDialog(window);

        if (file != null) {
            System.out.println("сохранение файла");
            FileWorker fileWorker = new FileWorker(file);
            String taskType = (String) taskComboBox.getSelectionModel().getSelectedItem();
            String data = inputDataTextArea.getText();
            try {
                fileWorker.writeToTxt(taskType, data);
//                Уведомление об успешном сохранении
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setTitle(APP_TITLE);
                alert.setHeaderText("Успех!");
                alert.setContentText("Файл успешно сохранён");
                alert.showAndWait();
            } catch (IOException ex) {
                System.out.println("Something is wrong with write to txt file");
                ex.printStackTrace();
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setTitle(APP_TITLE);
                alert.setHeaderText("Исключение!");
                alert.setContentText("Что-то пошло не так при сохранении файла");
                alert.showAndWait();
            }
        }
    }

    @FXML
    /**
     * Обработчик нажатия кнопки загрузки
     */
    private void loadButtonPressed() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Открыть");
        //установка фильтра только на текстовые файлы
        FileChooser.ExtensionFilter extFilter =
                new FileChooser.ExtensionFilter("Текстовые файлы (*.txt)", "*.txt");
        fileChooser.getExtensionFilters().add(extFilter);
        Stage window = (Stage) loadButton.getScene().getWindow();
        //получение файла (пути) из диалога
        File file = fileChooser.showOpenDialog(window);
        if (file != null) {
            System.out.println("чтение файла");
            FileWorker fileWorker = new FileWorker(file);
            try {

                /*По-хорошему, надо бы сделать проверку на соответствие файла заданному формату, с т.з. содержимого.
                Но, пока что, не приоритетно.
                 */

                //получаем данные из файла
                String[] readData = fileWorker.loadFromTxt();
                //выставляем данные на форму
                taskComboBox.getSelectionModel().select(readData[0]);
                inputDataTextArea.setText(readData[1]);
            } catch (IOException e) {
                System.out.println("Что-то пошло не так при загрузке данных из файла");
                e.printStackTrace();
            }
        }
    }

    @FXML
    /**
     * Обработчик нажатия на кнопку подсчёта
     */
    private void calculateButtonPressed() {
        try {
            //проверка выбора типа задачи
            int selectedTaskIndex = taskComboBox.getSelectionModel().getSelectedIndex();
            if (selectedTaskIndex == -1) {
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setTitle(APP_TITLE);
                alert.setHeaderText("Не выбрана задача");
                alert.setContentText("Выберите задачу из выпадающего списка");
                alert.showAndWait();
                return;
            }
            //проверка заполненности текстовой области с входными данными
            if (inputDataTextArea.getText().isEmpty()) {
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setTitle(APP_TITLE);
                alert.setHeaderText("Не заполнено поле входных данных");
                alert.setContentText("Заполните поле или выполните загрузку данных");
                alert.showAndWait();
                return;
            }
            DataConverter converter = new DataConverter();
            switch (selectedTaskIndex) {
                case 0:
                    //Подготовка данных для решения задачи 1
                    //Вынесение преобразований данных в специальный класс Конвертер
                    String[][] preparedData = converter.prepareDataForTask1(inputDataTextArea.getText());
                    //Задание исходных строковых массивов в класс решения задачи №1
                    Task1 task1 = new Task1(preparedData[0], preparedData[1]);
                    String[] resArrStr = task1.parseArrStr();
                    resArrStr = task1.sortResArrStr(resArrStr);
                    resultDataTextArea.setText(java.util.Arrays.toString(resArrStr));
                    break;
                case 1:
                    System.out.println("Подсчёт для задачи 2");
                    String dataForTask2 = converter.prepareDataForTask2(inputDataTextArea.getText());
                    Task2 task2 = new Task2(dataForTask2);
                    String res = task2.expandWholeNumber();
                    resultDataTextArea.setText(res);
                    break;
            }
//        } catch (MoreOneSpaceException ex) {
//            ex.printStackTrace();
//            Alert alert = new Alert(Alert.AlertType.INFORMATION);
//            alert.setTitle(APP_TITLE);
//            alert.setHeaderText("Некорректные данные");
//            alert.setContentText("Входные данные содержат больше одного пробела подряд между словами.");
//            alert.showAndWait();
//            return;
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }


}
