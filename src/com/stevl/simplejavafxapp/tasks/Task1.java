package com.stevl.simplejavafxapp.tasks;

import com.stevl.simplejavafxapp.DataConverter;

import java.util.ArrayList;
import java.util.List;

/**
 * Описывает решение задачи из файла 1.txt
 * <p>
 * Given two arrays of strings a1 and a2 return a sorted array r in lexicographical order of the strings of a1 which
 * are substrings of strings of a2.
 * </p>
 * <p>
 * Example 1:
 * a1 = ["arp", "live", "strong"]
 * a2 = ["lively", "alive", "harp", "sharp", "armstrong"]
 * returns ["arp", "live", "strong"]
 * </p>
 * <p>
 * Example 2:
 * a1 = ["tarp", "mice", "bull"]
 * a2 = ["lively", "alive", "harp", "sharp", "armstrong"]
 * returns []
 * </p>
 * <p>Beware: r must be without duplicates.</p>
 */
public class Task1 {

    /**
     * Массив строк-шаблонов, которые нужно искать в другом массиве
     */
    private final String[] orderArrStr;
    /**
     * Массив строк, по которому осуществляется поиск строк-шаблонов.
     */
    private final String[] workingArrStr;

    /**
     * @param orderArrStr   Массив строк-шаблонов, которые нужно искать в другом массиве.
     * @param workingArrStr Массив строк, по которому осуществляется поиск строк-шаблонов.
     */
    public Task1(String[] orderArrStr, String[] workingArrStr) {
        this.orderArrStr = orderArrStr;
        this.workingArrStr = workingArrStr;
    }


    /**
     * Ищёт вхождения строк-шаблонов в рабочем массиве.
     * Примечание. А если найдены не все шаблоны в рабочем массиве, т.е. например, 2 из 3?
     *
     * @return Unsorted string array found substrings
     */

    public String[] parseArrStr() {
        List<String> resList = new ArrayList<String>();
        for (int cnt = 0; cnt < orderArrStr.length; cnt++) {
            for (int j = 0; j < workingArrStr.length; j++) {
                //проверяем содержится ли в слове рабочего массива слово из массива с шаблонами
                if (workingArrStr[j].contains(orderArrStr[cnt])) {
                    //если подстрока не содержится в результате, то добавляем её
                    if (!resList.contains(orderArrStr[cnt])) {
                        resList.add(orderArrStr[cnt]);
                    }
                }
            }
        }
        //Конвертируем список в требуемый результирующий массив строк
        String[] resArrStr = resList.toArray(new String[resList.size()]);
//        DataConverter.printArrStr(resArrStr);
        return resArrStr;
    }

    /**
     * Сортирует лексикографически (по алфавиту)
     */
    public String[] sortResArrStr(String[] arrStr) {
        java.util.Arrays.sort(arrStr);
        return arrStr;
    }


}
